// Folders
def workspaceFolderName = "${WORKSPACE_NAME}"
def projectFolderName = "${PROJECT_NAME}"
def samplejobName = projectFolderName + "/SampleFolder"
def samplejob = folder(samplejobName) {displayName('Folder created by Jenkins')}

//Jobs
def job1 = freeStyleJob(samplejobName + "/FirstJob")
def job2 = freeStyleJob(samplejobName + "/SecondJob")

def samplepipeline= buildPipelineView(samplejobName + "/Sample-Pipeline-Demo")

samplepipeline.with{	
	title('Pipeline Demo')
	displayedBuilds(5)
	selectedJob(samplejobName + "/FirstJob")
	showPipelineParameters()
	refreshFrequency(5)
	}
	
//Job Configurations
job1.with {
		scm {
			git {
				remote {
					url('https://faechavez@bitbucket.org/faechavez/francis.git')
					credentials("adop-jenkins-master")
				}
					branch("*/master")
				}
			}
	steps{
shell('''#!/bin/sh
	ls -lart''')
}
publishers{
	downstreamParameterized{
		trigger(samplejobName + "/SecondJob"){
			condition("SUCCESS")
			parameters{
				predefinedProp("CUSTOM_WORKSPACE",'$WORKSPACE')
				}
				}
				}
				}
        }






job2.with{
	parameters{	
		stringParam("CUSTOM_WORKSPACE","","")
	}
	steps{
	shell('''
	#!/bin/sh
	ls -lart $CUSTOM_WORKSPACE
	''')
	}
	}
	
	

